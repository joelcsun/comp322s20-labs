#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define PAGE_SIZE 4096

typedef unsigned long long int llu;


int main(int argc, char* argv[]){
	if(argc < 2){
		fprintf(stderr, "Address as first argument is required.\n");
		return 1;
	}

	char* input = argv[1];
	char* invalidInput;
	llu address = strtoull(input, &invalidInput, 10);

	/* `strtoull` assigns the position of first invalid character read.
	 * If it's the end of the string (null terminator), we're fine.
	 * */
	if(invalidInput != NULL && *invalidInput != '\0'){
		fprintf(stderr, "Invalid character read at position %li.\n", invalidInput - input);
		return 1;
	}

	llu page = (address / PAGE_SIZE);
	llu offset = (address % PAGE_SIZE);

	printf("The address %llu contains:\n", address);
	printf("page number = %llu\noffset = %llu\n", page, offset);

}
