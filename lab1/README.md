# LAB 1: Time and Forking Children

# Status

The `time-4-baby-and-me` is complete and works as defined by the document.

## Change log

* Add `Makefile` and `main.c`.
* Print process START and STOP time.
* Fork process, store PID and PPID.
* Check whether process is child or parent:
	* If child, print PID and PPID. Then `exit(0)`. (return value `0`)
	* If parent, wait for child to exit, and store return value. Then print PID, PPID, CID, and RETVAL.
* Print parent and child user/system time.
	* This usually prints `0` for all values since it's very quick.
	* We could increase user time by creating a busy loop.
* Update `Makefile` to match provided structure.

## Sample output
```
$ ./time-4-baby-and-me
START: 1582960189
PID: 180305, PPID: 180304
PID: 180304, PPID: 103374, CPID: 180305, RETVAL: 0
USER: 0, SYS: 0
CUSER: 0, CSYS: 0
STOP: 1582960189
```
