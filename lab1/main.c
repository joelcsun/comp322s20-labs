#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/wait.h>


void fork_and_print(){

	pid_t child_pid = fork();
	pid_t proc_pid  = getpid();
	pid_t proc_ppid = getppid();

	if(child_pid == 0){
		/* We are the child process. */

		printf("PID: %d, PPID: %d\n", proc_pid, proc_ppid);
		// We could add a busy loop here to increase child user time.
		// int i = 2000000000;
		// while(i--){}
		exit(0);

	} else {
		/* We are not the child process. */

		int status;
		waitpid(child_pid, &status, 0);
		printf("PID: %d, PPID: %d, CPID: %d, RETVAL: %d\n", proc_pid, proc_ppid, child_pid, status);
	}
}

void print_times(){
	struct tms buf;
	times(&buf);
	printf("USER: %li, SYS: %li\n", buf.tms_utime, buf.tms_stime);
	printf("CUSER: %li, CSYS: %li\n", buf.tms_cutime, buf.tms_cstime);
}

int main(void){
	printf("START: %li\n", time(NULL));

	fork_and_print();
	print_times();

	printf("STOP: %li\n", time(NULL));
}
