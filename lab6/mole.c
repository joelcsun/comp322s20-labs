#include <errno.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


int main(int argc, char* argv[]){

	/* Find home directory to open log file in */
	char* home = getenv("HOME");
	if(home == NULL)
		home = getpwuid(getuid())->pw_dir;
	if(home == NULL){
		fprintf(stderr, "Warning: Could not determine home directory, falling back to /\n");
		home = "";
	}

	/* Build file name from home directory */
	char filename[256];
	sprintf(filename, "%s/lab6.log", home);

	/* Open file to append additional text */
	FILE* logfile = fopen(filename, "a");
	if(logfile == NULL){
		perror("Could not open log file");
		return errno;
	}

	fprintf(logfile, "Pop %s\n", argv[0]);
	fclose(logfile);
}
