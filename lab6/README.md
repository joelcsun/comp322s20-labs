# LAB 6: Wack a Mole

## Changelog

* Add daemon preparation code as described by book.
* Add `mole` executable that opens file to append message.
* Implement signal listeners
* Implement signal handlers, fork process and exec.
