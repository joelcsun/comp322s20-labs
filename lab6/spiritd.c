#define _POSIX_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


pid_t mole[2];


void exit_err(const char* msg){
	perror(msg);
	exit(1);
}

void register_signal(int signum, void(*handler)(int)){
	if(signal(signum, handler) == SIG_ERR){
		exit_err("Failed to register signal");
	}
}

void handle_sigterm(){
	for(int i = 0; i < 2; i++){
		if(mole[i] > 0)
			kill(mole[i], SIGKILL);
	}
	exit(0);
}

void handle_sigusr(int signum){
	register_signal(signum, handle_sigusr);

	int target = (signum == SIGUSR1) ? 0 : 1;
	if(mole[target] > 0){
		if(kill(mole[target], SIGKILL) < 0)
			perror("Could not kill child process");
		else
			mole[target] = 0;
	}

	int pick = rand() % 2;
	if(mole[pick] != 0)
		return; //There's already a mole in this slot, skip.

	pid_t child = fork();
	if(child == -1){
		//Error
		perror("Could not fork process");
	} else if(child > 0){
		//Parent
		mole[pick] = child;
		printf("FORKED %d\n", child);
	} else if(child == 0){
		//Child
		char name[6];
		sprintf(name, "mole%d", pick + 1);

		char* args[] = { name, NULL };
		int result = execv("./mole", args);
		if(result == -1){
			perror("Failed to launch mole");
		}
		//exit(0);
	}
}

void daemonize(){
	// Set file mode creation mask to 0
	umask(0);

	// Fork and exit parent
	pid_t child_pid = fork();
	if(child_pid < 0)
		exit_err("Failed to fork process");
	else if(child_pid != 0)
		exit(0);

	// Create new session and process group
	if(setsid() == -1)
		exit_err("Failed to create new session");

	// Change current working directory to root
	if(chdir("/") == -1)
		exit_err("Failed to change working directory"); 

	// Close all file descriptors
	struct rlimit rlim;
	if(getrlimit(RLIMIT_NOFILE, &rlim) == -1)
		exit_err("Failed to get maximum file descriptor count");
	for(rlim_t i = 0; i < rlim.rlim_cur; i++)
		close(i);

	int fd0 = open("/dev/null", O_RDWR);
	int fd1 = dup(0);
	int fd2 = dup(0);
	if(fd0 != 0 || fd1 != 1 || fd2 != 2){
		fprintf(stderr, "stdin, stdout, or stderr were not properly assigned\n");
		exit(1);
	}
}

int main(void){

	mole[0] = 0;
	mole[1] = 0;
	srand(getpid());

	daemonize();

	register_signal(SIGTERM, handle_sigterm);
	register_signal(SIGUSR1, handle_sigusr);
	register_signal(SIGUSR2, handle_sigusr);

	while(1){
		pause();
	}
}
