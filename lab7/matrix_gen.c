#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define RANGE 100
#define BUF_SIZE 1024

size_t parse_value(char* val){
	char* invalidInput;
	size_t result = strtoull(val, &invalidInput, 10);
	if(invalidInput != NULL && *invalidInput != '\0'){
		fprintf(stderr, "Invalid character read at position %li.\n", invalidInput - val);
		exit(1);
	}
	return result;
}

int main(int argc, char* argv[]){
	if(argc < 2){
		fprintf(stderr, "Usage: matrix_gen <N>\n");
		fprintf(stderr, "    N: size for the N*N matrix\n");
		return 1;
	}
	size_t input = parse_value(argv[1]);
	size_t remaining = (input * input);
	srand(getpid());

	// We do not want to call `write` for each random number,
	// so define buffer to hold the numbers in bulk.
	int buf[BUF_SIZE];
	while(remaining > 0){
		size_t current = (remaining < BUF_SIZE) ? remaining : BUF_SIZE;
		for(size_t i = 0; i < current; i++){
			int num = (rand() % RANGE*2) - RANGE;
			buf[i] = num;
		}
		remaining -= current;
		size_t bytes = sizeof(*buf) * current;
		write(STDOUT_FILENO, buf, bytes);
	}

	return 0;
}
