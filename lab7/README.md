# LAB 7: Asynchronous Elephant

## Changelog

* Add `Makefile`
* Add `matrix_gen` script
* Re-write `matrix_gen` script to output binary data
* Wave the wand.
* Do some magic.
* Okay, it's done.
