#include <aio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>


int scalar;
int vector_length;
int block_length;
size_t block_bytes;


void matrix_add(int* buffer, size_t bytes){
	int num = (bytes / sizeof(int));
	for(int i = 0; i < num; i++){
		buffer[i] += scalar;
	}
}

int read_with(struct aiocb* cb, off_t offset){
	cb->aio_fildes = STDIN_FILENO;
	cb->aio_offset = offset;
	cb->aio_nbytes = block_bytes;
	return aio_read(cb);
}

int write_with(struct aiocb* cb, size_t max_bytes){
	cb->aio_fildes = STDOUT_FILENO;
	cb->aio_nbytes = max_bytes;
	return aio_write(cb);
}

void do_async(){
	// Initialize two AIO control blocks.
	struct sigevent no_evt = { .sigev_notify = SIGEV_NONE };
	struct aiocb cb[2] = { 0 };
	for(int i = 0; i < 2; i++){
		int* buffer = malloc(block_bytes);
		if(buffer == NULL){
			fprintf(stderr, "Failed to allocate memory, exiting.");
			exit(1);
		}
		cb[i].aio_buf = buffer;
		cb[i].aio_reqprio = 1;
		cb[i].aio_sigevent = no_evt;
	}

	/*  I did not understand the reasoning behind the example pipeline strategy.
	 *  To improve performance, I wanted to avoid using memcpy
	 *  and simply do the matrix addition in the buffer.
	 *  This also lets us instantly write the buffer right back out.
	 *
	 *  My approach:
	 *    Two aiocbs (A and B) that take turns reading and writing.
	 *    When read on aiocb[A] is done, instantly queue read on aiocb[B].
	 *    In the mean time, perform matrix addition in aiocb[A]'s buffer.
	 *    Then queue write for the now modified aiocb[A].
	 *    Rinse and repeat. This should mean that there is always a read
	 *    going on asynchronously.
	 **/
	int reader = 0;
	off_t offset = 0;
	read_with(&cb[reader], offset);

	while(1){
		if(aio_error(&cb[reader]) != 0 || aio_error(&cb[1]) != 0)
			continue;

		int bytesRead = aio_return(&cb[reader]);
		offset += bytesRead;
		if(bytesRead == 0)
			break;
		
		read_with(&cb[!reader], offset);
		matrix_add((int*) cb[reader].aio_buf, bytesRead);
		write_with(&cb[reader], bytesRead);

		reader = !reader;
	}
}

int parse_value(char* val){
	char* invalidInput;
	int result = strtol(val, &invalidInput, 10);
	if(invalidInput != NULL && *invalidInput != '\0'){
		fprintf(stderr, "Invalid character read at position %li.\n", invalidInput - val);
		exit(1);
	}
	return result;
}

int main(int argc, char* argv[]){
	if(argc < 3){
		fprintf(stderr, "Usage: matrix_add <size> <blocks>\n");
		return 1;
	}
	int matrix_size = parse_value(argv[1]);
	int blocks = parse_value(argv[2]);
	if(matrix_size % blocks != 0){
		fprintf(stderr, "Bad block count: Does not divide into matrix size.\n");
		return 1;
	}
	block_length = (matrix_size / blocks);
	block_bytes = sizeof(int) * block_length;
	vector_length = (matrix_size * matrix_size);

	srand(getpid());
	scalar = rand() % 100;

	struct timespec start, end;
	clock_gettime(CLOCK_REALTIME, &start);

	do_async();

	clock_gettime(CLOCK_REALTIME, &end);
	double diff = 1.0 * (end.tv_sec - start.tv_sec) + 0.000000001 * (end.tv_nsec - start.tv_nsec);
	fprintf(stderr, "Finished in %.2fs.\n", diff);
}
