# LAB 3: Catcher

## Overview

This folder contains the source code for the `catcher` executable. The project has been completed as defined by the specification sheet.

## Sample Usage

```
$ ./catcher TERM INT USR1
```

## Changelog

* Add `main.c`, print process ID.
* Add list of signals to catch (names and numbers).
* Add utility functions to associate signal names and numbers.
* Implement signal handler with logging.
* Parse command line arguments for signal names, register handlers.
* Add `Makefile`.
* Update documentation.
* Add error checking, remove `KILL` and `STOP` as potential signals to catch
