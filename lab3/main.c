#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>


const char* SIGNAL_NAMES[] = {
	"HUP",  "INT",   "QUIT", "ILL",  "TRAP", "ABRT", "BUS",    "FPE",
	"USR1", "SEGV",  "USR2", "PIPE", "ALRM", "TERM", "STKFLT", "CHLD",
	"CONT", "TSTP",  "TTIN", "TTOU", "URG",  "XCPU", "XFSZ",   "VTALRM",
	"PROF", "WINCH", "POLL", "PWR",  "SYS"
};
const int SIGNAL_VALUES[] = {
	SIGHUP,  SIGINT,   SIGQUIT, SIGILL,  SIGTRAP, SIGABRT, SIGBUS,    SIGFPE,
	SIGUSR1, SIGSEGV,  SIGUSR2, SIGPIPE, SIGALRM, SIGTERM, SIGSTKFLT, SIGCHLD,
	SIGCONT, SIGTSTP,  SIGTTIN, SIGTTOU, SIGURG,  SIGXCPU, SIGXFSZ,   SIGVTALRM,
	SIGPROF, SIGWINCH, SIGPOLL, SIGPWR,  SIGSYS
};
const int SIGNAL_COUNT = sizeof(SIGNAL_NAMES) / sizeof(*SIGNAL_NAMES);

void sig_handler(int signum);


const char* get_signame_from_num(int signum){
	for(int i = 0; i < SIGNAL_COUNT; i++){
		if(signum == SIGNAL_VALUES[i])
			return SIGNAL_NAMES[i];
	}
	return NULL;
}

int get_signum_from_name(char* signame){
	for(int i = 0; i < SIGNAL_COUNT; i++){
		if(strcmp(signame, SIGNAL_NAMES[i]) == 0)
			return SIGNAL_VALUES[i];
	}
	return -1;
}

void register_signal(int signum){
	if(signal(signum, sig_handler) == SIG_ERR){
		const char* signame = get_signame_from_num(signum);
		if(signame == NULL)
			fprintf(stderr, "Error registering unknown signal %d", signum);
		else
			fprintf(stderr, "Error registering signal '%s'", signame);
		fprintf(stderr, " (errno %d)\n", errno);
	}
}


void sig_handler(int signum){
	static int  total_count = 0;
	static int  term_count  = 0;
	time_t      now         = time(NULL);
	const char* signame     = get_signame_from_num(signum);

	total_count++;

	/* Print message, handle TERM specially */
	printf("SIG%s caught at %zu.", signame, now);
	if(signum == SIGTERM){
		term_count++;
		printf(" (%d of 3)\n", term_count);
	} else {
		term_count = 0;
		printf("\n");
	}

	/* Re-register this signal */
	register_signal(signum);

	/* Handle exit condition */
	if(term_count >= 3){
		fprintf(stderr, "Total signals count = %d\n", total_count);
		exit(0);
	}
}

void register_signals_from_args(int argc, char** argv){
	for(int i = 1; i < argc; i++){
		char* arg = argv[i];
		int signum = get_signum_from_name(arg);
		if(signum == -1){
			fprintf(stderr, "Skipping unknown signal \"%s\".\n", arg);
		} else {
			register_signal(signum);
		}
	}
}

int main(int argc, char* argv[]){
	/* Print program name and PID */
	char* self = argv[0];
	pid_t pid = getpid();
	fprintf(stderr, "%s: $$ = %d\n", self, pid);

	/* Parse args and register signal handlers */
	register_signals_from_args(argc, argv);

	/* Pause loop for signals */
	while(1){
		pause();
	}
}
