# LAB 4: Dining Philosophers

## Overview

The `dining-p` program demonstrates and solves the issue of deadlocks that can occur when the necessary conditions are present. In order to solve the problem, the approach is to use a critical-section lock where the circular-locking issue has the potential to occur.

## Usage

Run one eating philosopher at seat #1:
```
$ ./dining-p 1
```

Run all 5 philosophers at once:
```
$ ./run
```

## Changelog

* Add random sleep function, seed with `time(NULL)`.
* Implement dining-philosophers example from book.
* Read desired seat from command line arguments.
* Add simple semaphore initialization, acquire chopsticks before eating.
* Add `SIGTERM` handler, keep track of eat-think cycles.
* Add custom `Semaphore` struct to simplify opening, closing, unlinking semaphores.
* Only open the two semaphores (chopsticks) the chosen seat requires.
* Add an additional semaphore, `critical`, to surround the acquiring of both chopsticks.
	* This critical semaphore is shared between all `dining-p` processes, and successfully prevents deadlocks by ensuring only one philosopher grabs their chopsticks at a time.
