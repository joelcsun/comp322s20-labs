#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define N_SEATS 5
int seat;

typedef struct Semaphore {
	char name[32];
	sem_t* sem;
} Semaphore;
Semaphore critical, chopstickL, chopstickR;

volatile int terminating = 0;


void sleep_s(double min, double max){
	double seconds = min + ((max - min) * rand() / RAND_MAX);
	usleep(seconds * 1e6);
}

void eat(){

	sem_wait(critical.sem);
	sem_wait(chopstickL.sem);
	sem_wait(chopstickR.sem);
	sem_post(critical.sem);

	printf("Philosopher #%d is eating...\n", seat + 1);
	sleep_s(0.1, 0.5);

	sem_post(chopstickL.sem);
	sem_post(chopstickR.sem);
}

void think(){
	printf("Philosopher #%d is thinking...\n", seat + 1);
	sleep_s(0.1, 0.5);
}

void sig_handler(){
	terminating = 1;
}

void chopstick_open(Semaphore* target, int n){
	sprintf(target->name, "/phil_chopstick%d", n);

	sem_t* result = sem_open(target->name, O_CREAT | O_EXCL, 0666, 1);
	if(result == SEM_FAILED && errno == EEXIST){
		fprintf(stderr, "Semaphore '%s' already exists, opening instead.\n", target->name);
		result = sem_open(target->name, 0);
	}
	if(result == SEM_FAILED){
		fprintf(
			stderr, "Could not open semaphore '%s': %s. Aborting\n",
			target->name, strerror(errno)
		);
		exit(errno);
	}
	target->sem = result;
}

void initialize(){
	/* Seed the random number generator */
	srand(time(NULL) + seat * 1234);

	/* Initialize semaphores */
	chopstick_open(&chopstickL, seat);
	chopstick_open(&chopstickR, (seat + 1) % N_SEATS);
	chopstick_open(&critical  , N_SEATS);

	/* Register SIGTERM handler */
	if(signal(SIGTERM, sig_handler) == SIG_ERR){
		perror("Could not register SIGTERM handler.");
		exit(errno);
	}
}

void cleanup(){
	/* Close semaphores */
	sem_close(chopstickL.sem);
	sem_close(chopstickR.sem);
	sem_close(critical.sem);
	sem_unlink(chopstickL.name);
	sem_unlink(chopstickR.name);
	sem_unlink(critical.name);
}

int main(int argc, char* argv[]){
	if(argc < 2 || atoi(argv[1]) < 1 || atoi(argv[1]) > N_SEATS){
		fprintf(stderr, "Usage: %s <SEAT_NUMBER (1-%d)>\n", argv[0], N_SEATS);
		return 1;
	}

	seat = atoi(argv[1]) - 1;
	initialize();

	int cycles = 0;
	do {

		eat();
		think();
		cycles++;

	} while(!terminating);

	cleanup();
	fprintf(stderr, "Philosopher #%d completed %d cycles.\n", seat + 1, cycles);
	return 0;
}
