#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>

// Use extern to grab environment, using main is not POSIX compliant
extern char **environ;


/* Read from main arguments, split into two separate arrays at comma */
int split_argv(int argc, char* argv[], char** prog1, char** prog2){
	int k = 0;
	char** target = prog1;

	for(int i = 1; i < argc; i++){
		char* str = argv[i];
		if(str[0] == ',' && str[1] == '\0'){
			// NULL terminate first program array
			target[k] = NULL;
			// Set target to second program array
			target = prog2;
			k = 0;
		} else {
			target[k++] = str;
		}
	}
	if(target != prog2){
		// We never switched over to the second program,
		//   which means we never read a comma. Return error
		return 0;
	} else {
		// NULL terminate second program
		target[k] = NULL;
	}
	return 1; //SUCCESS
}

/* Fork 2 children from the parent, return whether successful or not */
int fork_twice(pid_t* pid1, pid_t* pid2){
	*pid1 = fork();
	if(*pid1 < 0){
		return 0; //FORK FAILED
	} else if(*pid1 > 0){
		*pid2 = fork();
		if(*pid2 < 0){
			return 0; //FORK FAILED
		}
	}
	return 1; //SUCCESS
}


int main(int argc, char* argv[]){
	// Allocate separate arrays to pass to execve. Argc will always be big enough.
	char* program1[argc];
	char* program2[argc];

	// Split our input args into two individuals
	int success = split_argv(argc, argv, program1, program2);
	if(!success){
		fprintf(stderr, "Error: Missing argument, no ',' was provided.\n");
		return 1;
	}
	// Create a pipe
	int pipefd[2];
	if(pipe(pipefd) == -1){
		fprintf(stderr, "Error: Pipe creation failed. (errno %d)\n", errno);
		return errno;
	}
	// Fork two children from parent
	pid_t child1, child2;
	success = fork_twice(&child1, &child2);
	if(!success){
		fprintf(stderr, "Error: Fork failed. (errno %d)\n", errno);
		return errno;
	}

	// Begin application logic
	if(child1 > 0 && child2 > 0){
		// Parent Process: print cpids, wait, print vals
		fprintf(stderr, "%s: $$ = %d\n", program1[0], child1);
		fprintf(stderr, "%s: $$ = %d\n", program2[0], child2);
		close(pipefd[0]);
		close(pipefd[1]);
		int status1, status2;
		waitpid(child1, &status1, 0);
		waitpid(child2, &status2, 0);
		fprintf(stderr, "%s: $? = %d\n", program1[0], status1);
		fprintf(stderr, "%s: $? = %d\n", program2[0], status2);

	} else if(child1 == 0){

		// Child 1: Wire stdout to redirect to pipe
		dup2(pipefd[1], STDOUT_FILENO);
        close(pipefd[1]);
		int status = execve(program1[0], program1, environ);
		return status;

	} else if(child2 == 0){

		// Child 2: Wire stdin to read from pipe
		dup2(pipefd[0], STDIN_FILENO);
        close(pipefd[0]);
		int status = execve(program2[0], program2, environ);
		return status;
	}

	return 0;
}
