# LAB 2: Launch Tube

## Overview

This folder contains the source for two executables: `launch` and `tube`. The project has been completed as defined by the specification sheet.

## Sample usage

### Launch
```
$ launch /bin/ls
/bin/ls: $$ = 26843
a.out  foo.c
/bin/ls: $? = 0
```

### Tube
```
$ tube /bin/cat filename , /usr/bin/head -2
/bin/cat: $$ = 26843
/bin/head: $$ = 26844
This is the first line
This is the second line
/bin/cat: $? = 0
/bin/head: $? = 0
```

## Changelog

* Add `launch.c` with basic forking.
* Add error-checking for arguments and forking.
* Finish `launch` implementation, executes with inherited arguments.
* Add `tube.c` with basic pipe creation.
* Fork two children from parent with error-checking. Print necessary info.
* Add logic to split input arguments at the comma, copy into new array.
* Properly format parent output now that we have program names separated.
* Finalize `tube.c`: Wire the pipes up, execute the commands in forked processes.
* Add `Makefile`, producing two executables.
