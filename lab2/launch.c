#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

// Use extern to grab environment, using main is not POSIX compliant
extern char **environ;


int main(int argc, char* argv[]){
	if(argc < 2){
		fprintf(stderr, "No command was specified.\n");
		return 1;
	}

	char* program = argv[1];
	pid_t child_pid = fork();

	if(child_pid < 0){
		fprintf(stderr, "Fork failed. Errno.: %d\n", errno);
		return errno;
	}

	if(child_pid == 0){
		/* Child Process */

		execve(program, argv + 1, environ);

	} else {
		/* Parent Process */

		fprintf(stderr, "%s: $$ = %d\n", program, child_pid);

		int status;
		waitpid(child_pid, &status, 0);

		fprintf(stderr, "%s: $? = %d\n", program, status);

	}
}
