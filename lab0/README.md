# LAB 0: Making Sense of Ones and Zeros

## Status

The `zero-one` command-line application has been completed in full. It includes error checking and messaging at all stages, and is flexible when reading the input.

## Change log

* Add `ASCII_MAP` string array to reference non-printable ASCII characters (<=32) by name.
* Implement `read_file` function to read all bytes from a file.
	* Includes error handling/messaging: opening file, reading file, buffer overflow.
* Read program arguments for filename, default to STDIN if none or `-` is provided.
* Ensure the file contents are not malformed: should contain only `1`,`0`, and white-space characters.
* Parse file contents:
	1. Read binary number strings, add right-padded zeros if necessary.
	2. Count bits for EVEN or ODD parity.
	3. Convert binary string into `int` data type.
	4. Find the associated ASCII name when the value is <= 32.
* Print formatted table with resulting values, properly padded.
* Use `isspace()` to check for all POSIX white-space characters.
* Add `Makefile`.
* Fix buffer null-termination.
* Use `ioctl` to check whether input is empty. (fixes blocking `read`)
* Move some error handling/returning to main function.
* Read content from command line arguments rather than stdin (if filename was not provided).
