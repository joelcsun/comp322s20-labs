#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_SIZE 4096

enum { EVEN, ODD };
typedef struct {
	char* binary_str;
	char* ascii;
	int value;
	int parity;
} NumberInfo;

const char* ASCII_MAP[] = {
	"NUL","SOH","STX","ETX","EOT","ENQ","ACK","BEL",
	"BS","HT","LF","VT","FF","CR","SO","SI",
	"DLE","DC1","DC2","DC3","DC4","NAK","SYN","ETB",
	"CAN","EM","SUB","ESC","FS","GS","RS","US","SPACE"
};

void read_args (int start_index, int argc, char* argv[], char* buf) {
	for(int i = start_index; i < argc; i++){
		strcat(buf, argv[i]);
		strcat(buf, " ");
	}
}

int read_file (char* file_name, char* buf) {
	int fp = open(file_name, O_RDONLY);
	if(fp == -1){
		fprintf(stderr, "Error opening file \"%s\": %s.\n", file_name, strerror(errno));
		return 0; //fail
	}

	int result = read(fp, buf, BUFFER_SIZE);
	if(result == -1){
		fprintf(stderr, "Error reading file: %s.\n", strerror(errno));
		return 0; //fail
	} else if(result == BUFFER_SIZE){
		fprintf(stderr, "Warning: maximum buffer read (%d bytes)\n", result);
		buf[BUFFER_SIZE - 1] = '\0';
	}
	return 1; //success
}

int valid_input (char* source) {
	int length = strlen(source);
	// Ensure all characters are either 0, 1, or whitespace
	for(int i = 0; i < length; i++){
		char c = source[i];
		if(c != '0' && c != '1' && !isspace(c)){
			return 0; //fail
		}
	}
	return 1; //success
}

char* read_number (char* source, char* output) {
	int i;
	// Copy the character bits to the output
	for(i = 0; i < 8; i++){
		char c = source[i];
		if(c == '0' || c == '1')
			output[i] = c;
		else
			break;
	}
	// Move read position to end of binary number string (input)
	source = source + i;
	// Fill in missing bits by right-padding with zeros (output)
	while(i < 8){
		output[i++] = '0';
	}
	// Null-terminate the output string
	output[8] = '\0';

	// Read away all the trailing whitespace
	while(isspace(source[0])){
		source += 1;
	}
	// If we reached end of string/file, return NULL pointer to signify finish
	if(source[0] == '\0')
		return NULL;

	// Return string at a position where next number can be read
	return source;
}

void parse_number (char* str, NumberInfo* output) {
	int parity = (str[0] == '1') ? ODD : EVEN;

	int value = 0;
	for(int i = 1; i < 8; i++){
		if(str[i] == '1'){
			// Sum up each bit with BITWISE OR, shifted to the correct position
			value |= 1 << (7 - i);
			// Flip odd/even for parity boolean
			parity = !parity;
		}
	}
	output->value = value;
	output->parity = parity;

	// Associate the printable string name to this ASCII character
	if(value <= 32){
		strcpy(output->ascii, ASCII_MAP[value]);
	} else if(value == 0x7F){
		strcpy(output->ascii, "DEL");
	} else {
		output->ascii[0] = value;
		output->ascii[1] = '\0';
	}
}

void parse_input (char* source) {
	char* position = source;

	// Read loop until we run out of numbers
	while(position != NULL){
		char number[9];
		// Read next number, zero-padded, into separate array
		// Then update position to next number
		position = read_number(position, number); 

		// Parse the number and write results into this struct
		char ascii[6];
		NumberInfo info = (NumberInfo){ number, ascii, 0, 0 };
		parse_number(number, &info);

		// Print values from the output struct
		printf("%8s %8s %8d %8s\n",
				info.binary_str,
				info.ascii,
				info.value,
				info.parity ? "ODD" : "EVEN"
		);
	}
}

int main (int argc, char* argv[]) {
	// Verify we have an argument to read (other than program name)
	if(argc <= 1){
		fprintf(stderr, "Error: No input was provided.\n");
		return 1; //exit as failure
	}

	// Initialize buffer where number string is stored
	char buf[BUFFER_SIZE] = {0};

	// Gather facts about input arguments
	int first_arg_is_dash   = (strcmp(argv[1], "-") == 0);
	int first_arg_is_number = valid_input(argv[1]);

	if(first_arg_is_dash){
		read_args(2, argc, argv, buf); //read args into buffer starting at index 2
	} else if(first_arg_is_number) {
		read_args(1, argc, argv, buf); //read args into buffer starting at index 1
	} else {
		int success = read_file(argv[1], buf);
		if(!success){
			return 1; //exit as failure
		}
	}

	// Ensure read content is valid
	if(!valid_input(buf)){
		fprintf(stderr, "Error: Input contains invalid characters.\n");
		return 1; //exit as failure
	}

	// Print header
	printf("Original ASCII    Decimal  Parity  \n");
	printf("-------- -------- -------- --------\n");

	// Parse contents, print each line
	parse_input(buf);
}
